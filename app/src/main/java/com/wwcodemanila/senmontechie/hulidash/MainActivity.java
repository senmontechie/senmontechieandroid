package com.wwcodemanila.senmontechie.hulidash;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button btnCheckLicense;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnCheckLicense = findViewById(R.id.btnCheckLicense);
        btnCheckLicense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Intent intentResult = new Intent(getApplication(), CheckLicense.class);
                startActivity(intentResult);
            }
        });
    }


}
